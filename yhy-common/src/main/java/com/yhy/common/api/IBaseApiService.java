package com.yhy.common.api;

import com.yhy.common.constants.BusSelfState;
import com.yhy.common.dto.workflow.WorkflowInfo;

import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b>定义所有模块的API回调<br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-5-12 下午5:26 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public interface IBaseApiService {

    /*
    * 用于工作流结束时的回调,更新业务表状态
    *  返回状态
     */
    Boolean updateWorkflowSelfState(String userAccount, BusSelfState busSelfState, WorkflowInfo workflowInfo);

}
