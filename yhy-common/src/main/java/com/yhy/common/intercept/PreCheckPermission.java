package com.yhy.common.intercept;

import com.google.common.collect.Sets;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Set;

/**
 * Created by yanghuiyuan on 2017/11/24.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface PreCheckPermission {

    String[] roles() default {""};

}
