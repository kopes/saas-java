package com.yhy.common.constants;

import com.yhy.common.exception.CustomRuntimeException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b> 权限功能代码<br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-24 下午5:02 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public enum BusPrivCode {

	/**
	 * 系统管理员
	 */
	PRIV_SYS_ADMIN("SYS_ADMIN"),

	/*
	* 默认管理员角色
	 */
	DFT_ADMIN_ROLE("DEFAULT_ADMIN"),

	/**
	 * 普通管理员
	 */
	PRIV_COMMON_ADMIN("COMMON_ADMIN");


	private final String val;

	private static Map<String, BusPrivCode> busMap;

	BusPrivCode(String val) {
		this.val = val;
	}

	public String getVal() {
		return val;
	}

	/**
	 * 根据操作类型的值获取业务类型
	 * @param val
	 * @return
	 */
	public static BusPrivCode getInstByVal(String val) {
		if(busMap == null) {
			synchronized (BusPrivCode.class) {
				if (busMap == null) {
					busMap = new HashMap<String, BusPrivCode>();
					for (BusPrivCode moduleType : BusPrivCode.values()) {
						busMap.put(moduleType.getVal(), moduleType);
					}
				}
			}
		}

		if (!busMap.containsKey(val)) {
			throw new CustomRuntimeException("ModuleType模块值" + val + "对应枚举值不存在。");
		}
		return busMap.get(val);
	}

}
