package com.yhy.common.dao;

import com.yhy.common.dto.BaseEntity;
import org.apache.ibatis.annotations.Param;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import java.util.List;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

public interface BaseDao<T extends BaseEntity> {

    List<T> findAll();

    List<T> findBy(@Param("paramBean") T entity);

    T findById(@Param("id") String id);

    int deleteBy(@Param("paramBean")T entity);

    int deleteById(T entity);

    int updateNotEmpty(T entity);

    int update(T entity);

    int insert(T entity);

    void batchInsert(@Param("entities") List<T> entities);

    void batchUpdate(@Param("entities") List<T> entities);

    Long queryPageCount(T baseDTO);

    List<T> queryPage(T baseDTO);

}
