package com.yhy.common.dto;

import com.yhy.common.utils.ToStringBean;

import java.io.Serializable;
import java.util.Date;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-29 下午3:53 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *  * <b> 主表
 *
 */
public class BaseMainEntity extends BaseEntity {

    private String busModule;
    private String busClass;
    private String selfState;

    public String getSelfState() {
        return selfState;
    }

    public void setSelfState(String selfState) {
        this.selfState = selfState;
    }

    public String getBusModule() {
        return busModule;
    }

    public void setBusModule(String busModule) {
        this.busModule = busModule;
    }

    public String getBusClass() {
        return busClass;
    }

    public void setBusClass(String busClass) {
        this.busClass = busClass;
    }
}
