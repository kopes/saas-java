package com.yhy.common.exception;

public class BusinessException extends CustomRuntimeException {


	/**
	 * 错误代码
	 */
	private String errorCode;

	private Object[] params;

	public BusinessException(String errorCode, Object... params) {
		super(errorCode);
		this.errorCode = errorCode;
		this.params = params;
	}

	public BusinessException(String errorCode, Throwable throwable) {
		super(errorCode, throwable);
		this.errorCode = errorCode;
		this.params = new Object[0];
	}

	public BusinessException(String errorCode, Object[] params, Throwable throwable) {
		super(errorCode, throwable);
		this.errorCode = errorCode;
		this.params = params;
	}

	public String getErrorCode() {
		return errorCode;
	}

	@Override
	public String getMessage() {
		/*if (StringUtils.isNotBlank(errorCode)) {
			return I18nUtils.getMessage(errorCode, params);
		}*/
		return super.getMessage();

	}
}
