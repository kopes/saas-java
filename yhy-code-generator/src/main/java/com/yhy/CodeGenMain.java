package com.yhy;

import cn.org.rapid_framework.generator.Generator;
import cn.org.rapid_framework.generator.GeneratorFacade;
import cn.org.rapid_framework.generator.GeneratorProperties;

public class CodeGenMain
{
    public static void main(String[] args) throws Exception {
        String templateType = "1";
        generate(templateType, "T_WORKFLOW_APPROVE_DETAIL");
    }

    /**
     *
     * @param tables 表名
     * @throws Exception
     */
    public static void generate(String templateType, String... tables) throws Exception {
        Generator generator = new Generator();
        generator.addTemplateRootDir("yhy-code-generator/src/main/resources/template");
        generator.setOutRootDir(GeneratorProperties.getProperty("outRoot"));
        GeneratorFacade gf = new GeneratorFacade();
        gf.setGenerator(generator);
        gf.deleteOutRootDir();	   //s删除生成器的输出目录
        gf.generateByTable(tables); //通过数据库表生成文件,template为模板的根目录
    }

}
