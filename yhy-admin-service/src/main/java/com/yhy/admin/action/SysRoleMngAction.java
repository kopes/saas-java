package com.yhy.admin.action;

import com.google.common.collect.Sets;
import com.yhy.admin.dto.SysRoleDTO;
import com.yhy.admin.dto.SysUserDTO;
import com.yhy.admin.service.SysRoleFormService;
import com.yhy.admin.service.SysRoleService;
import com.yhy.admin.service.SysRoleUserService;
import com.yhy.admin.service.mng.SysRoleMngService;
import com.yhy.admin.vo.SysRoleFormVO;
import com.yhy.admin.vo.SysRoleUserVO;
import com.yhy.admin.vo.SysRoleVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.constants.BusPrivCode;
import com.yhy.common.constants.BusSelfState;
import com.yhy.common.constants.FunProcess;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-9 下午3:22 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/roleMng", produces="application/json;charset=UTF-8")
public class SysRoleMngAction extends BaseMngAction<SysRoleDTO> {

    @Autowired
    private SysRoleFormService sysRoleFormService;

    @Autowired
    private SysRoleUserService sysRoleUserService;

    @Override
    protected SysRoleMngService getBaseService() {
        return SpringContextHolder.getBean(SysRoleMngService.class);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysRoleDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysRoleDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @Override
    protected void beforeOfSaveProcess(SysRoleDTO baseDTO) {
        super.beforeOfSaveProcess(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_CHANGE","ROLE_CREATE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_SELECT","ROLE_CHANGE","ROLE_DELETE","ROLE_CREATE"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/saveMenu",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存角色与菜单关系", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg saveMenu(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return getBaseService().saveMenu(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/saveOrg",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存角色与组织关系", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg saveOrg(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return getBaseService().saveOrg(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/saveUser",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存角色与用户关系", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg saveUser(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return getBaseService().saveUser(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_DELETE"})
    @RequestMapping(value="/deleteRoleUser",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除角色与用户关系", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg deleteRoleUser(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        List<SysRoleUserVO> roleUserVOS = baseDTO.getBusMainData().getRoleUserVOList();
        for (SysRoleUserVO tmpVO : roleUserVOS) {
            sysRoleUserService.deleteById(tmpVO);
        }
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"");
        return returnMsg;
    }

    @RequestMapping(value="/findFormByRoleId",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据角色获取表单信息", notes="根据角色获取表单信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织", required = true, dataType = "String")

    })
    public AppReturnMsg findFormByRoleId(HttpServletRequest request, HttpServletResponse response,
                                         @RequestBody SysRoleDTO baseDto) {
        List<SysRoleFormVO> roleFormVOList = sysRoleFormService.findFormByRoleId(baseDto.getParamBean().getId());
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", roleFormVOList);
        return returnMsg;
    }


    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/saveRoleForm",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存角色与表单关系", notes="保存角色与表单关系")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg saveRoleForm(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        return getBaseService().saveRoleForm(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_DELETE"})
    @RequestMapping(value="/deleteRoleForm",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除角色与用户关系", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg deleteRoleForm(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        List<SysRoleFormVO> roleFormVOList = baseDTO.getBusMainData().getRoleFormVOList();
        for (SysRoleFormVO tmpVO : roleFormVOList) {
            sysRoleFormService.deleteById(tmpVO);
        }
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"");
        return returnMsg;
    }

    @RequestMapping(value="/findUserByRoleId",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据角色获取用户信息", notes="根据公司及组织获取用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司", required = true, dataType = "String"),
            @ApiImplicitParam(name = "orgCode", value = "组织", required = true, dataType = "String")

    })
    public AppReturnMsg findUserByRoleId(HttpServletRequest request, HttpServletResponse response,
                                         @RequestBody SysRoleDTO baseDto) {
        List<SysUserMngVO> sysUsers = sysRoleUserService.findUserByRoleId(baseDto.getParamBean().getId());
        SysUserDTO rtnDto = new SysUserDTO();
        rtnDto.setRtnList(sysUsers);
        AppReturnMsg returnMsg = new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",rtnDto);
        return returnMsg;
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_ALL","ROLE_CREATE","ROLE_CHANGE"})
    @RequestMapping(value="/joinAdminUser",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="加入管理员角色", notes="加入管理员角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "SysRoleDTO")
    })
    public AppReturnMsg joinAdminUser(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO baseDTO) {
        if(StringUtils.isBlank(baseDTO.getBusMainData().getSysOwnerCpy())) {
            throw new BusinessException("请为该用户设定所属公司");
        }
        SysRoleService sysRoleService = SpringContextHolder.getBean(SysRoleService.class);
        SysRoleVO paramBean = new SysRoleVO();
        paramBean.setEnableFlag("Y");
        paramBean.setSysOwnerCpy(baseDTO.getBusMainData().getSysOwnerCpy());
        paramBean.setAttribute1(BusPrivCode.DFT_ADMIN_ROLE.getVal());
        List<SysRoleVO> roleVOList = sysRoleService.findBy(paramBean);
        if(CollectionUtils.isEmpty(roleVOList)) {
            throw new BusinessException("未找到默认管理员角色");
        }
        String roleId = roleVOList.get(0).getId();
        baseDTO.getBusMainData().setId(roleId);
        return getBaseService().saveUser(baseDTO);
    }

    @RequestMapping(value="/findRoleByCpyCode",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="根据公司获取所有用户信息", notes="根据公司获取所有用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司", required = true, dataTypeClass = SysUserDTO.class),

    })
    public AppReturnMsg findUserByCpyCode(HttpServletRequest request, HttpServletResponse response,
                                          @RequestBody SysRoleDTO baseDTO) {
        baseDTO.setRtnList(getBaseService().queryPageData(baseDTO));
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",baseDTO);
    }


    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ROLE_SELECT","ROLE_CHANGE","ROLE_DELETE","ROLE_CREATE"})
    @RequestMapping(value="/findAllPrivList",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询权限清单", notes="查询权限清单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass = SysRoleDTO.class)
    })
    public AppReturnMsg findAllPrivList(HttpServletRequest request, HttpServletResponse response, @RequestBody SysRoleDTO sysRoleDTO) {
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",getBaseService().findAllPrivList(YhyUtils.getSysUser().getCpyCode(),
                sysRoleDTO.getUserParamBean().getUserAccount()));
    }

}
