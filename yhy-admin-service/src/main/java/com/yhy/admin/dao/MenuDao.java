package com.yhy.admin.dao;

import com.yhy.admin.vo.MenuVO;
import com.yhy.admin.vo.OrgSetVO;
import com.yhy.common.dao.BaseDao;
import com.yhy.common.dao.BaseMainDao;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-1 下午4:37 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "menuDao")
public interface MenuDao extends BaseMainDao<MenuVO> {


    List<MenuVO> findAllNotAdminMenu();
}
