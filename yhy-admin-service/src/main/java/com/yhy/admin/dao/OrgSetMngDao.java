package com.yhy.admin.dao;

import com.yhy.admin.dto.OrgSetDTO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Mapper
@Component(value = "orgSetMngDao")
public interface OrgSetMngDao extends BaseMngDao<OrgSetDTO,OrgSetMngVO> {

    List<OrgSetMngVO> findOrgSetByUserCpyCode(@Param("userAccount") String userAccount, @Param("cpyCode") String cpyCode);

    List<OrgSetMngVO> getOrgsetByCode(@Param("orgCodes") Set<String> orgCodes);

}
