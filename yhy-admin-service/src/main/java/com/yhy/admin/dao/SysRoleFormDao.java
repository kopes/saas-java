package com.yhy.admin.dao;

import com.yhy.admin.vo.SysRoleFormVO;
import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-20 上午9:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysRoleFormDao")
public interface SysRoleFormDao  extends BaseDao<SysRoleFormVO> {


    List<SysRoleFormVO> findFormByRoleId(@Param("roleId")String roleId);

    Set<String> findFormByRoleIds(@Param("roleIds")Set<String> roleIds);

}
