package com.yhy.admin.service.mng;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.yhy.admin.dto.RedisDTO;
import com.yhy.admin.vo.mng.RedisMngVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.dao.BaseMngDao;
import com.yhy.common.dto.BaseEntity;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseMainService;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.utils.RedisUtil;
import com.yhy.common.vo.PageVO;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.data.redis.connection.DataType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:25 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-14 下午5:26 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class RedisMngService extends BaseMngService<RedisDTO,RedisMngVO> {


    @Override
    public BaseMainService getBaseMainService() {
        return null;
    }

    @Override
    protected BaseMngDao getBaseMngDao() {
        return null;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_REDIS_MNG.getVal();
    }


    @Override
    public void doSave(RedisDTO baseDTO) {

    }

    @Override
    public void loadData(RedisDTO baseDTO) {
        List<RedisMngVO> rtnList = Lists.newArrayList();

        Set<String> keys = RedisUtil.getRedisTemplate().keys("*" + (baseDTO.getParamBean().getKey() == null ? "" : baseDTO.getParamBean().getKey()) + "*");
        PageVO pageVO = baseDTO.getPageBean();
        if (!CollectionUtils.isEmpty(keys)) {
            int i = 1;
            for (String key : keys) {
                if(pageVO.getStartRow() > i) {
                    continue;
                }
                RedisMngVO mngVO = new RedisMngVO();
                DataType type = RedisUtil.getRedisTemplate().type(key);
                Object value = null;
                try {
                    if(type == DataType.HASH) {
                        value = RedisUtil.hGet(key);
                    } else {
                        value = RedisUtil.get(key);
                    }
                } catch (Exception e) {
                    LOGGER.error(key + "获取异常", e);
                    value = "获取异常" + e.getMessage();
                }
                mngVO.setKey(key);
                mngVO.setValue(value == null ? "" : value.toString());
                rtnList.add(mngVO);
                if(pageVO.getEndRow() == i) {
                    break;
                }
                i++;
            }
        }
        pageVO.setTotalCount(keys.size());
        baseDTO.setRtnList(rtnList);
    }

    @Override
    public void toViewData(RedisDTO baseDTO) {
        RedisMngVO busMainData = new RedisMngVO();
        busMainData.setValue(String.valueOf(RedisUtil.get(baseDTO.getParamBean().getId())));

        busMainData.setKey(baseDTO.getParamBean().getId());
        baseDTO.setBusMainData(busMainData);
    }
}
